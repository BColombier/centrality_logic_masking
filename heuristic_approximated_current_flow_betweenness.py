# -*- coding: utf-8 -*-
import collections as col
import numpy as np
import networkx as nx

def heuristic_approximated_current_flow_betweenness(g_nx, key_size, prim_in, exclude = "           ", epsilons = [10.0, 30.0, 60.0]):
    try:
        # print("Trying option 1 (fastest)")
        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                        normalized = False,
                                                                                                                        solver = "full",
                                                                                                                        dtype = np.float32,
                                                                                                                        epsilon = epsilons[0]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
    except:
        try:
            # print("Trying option 1 (fastest)")
            list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                            normalized = False,
                                                                                                                            solver = "full",
                                                                                                                            dtype = np.float32,
                                                                                                                            epsilon = epsilons[1]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
        except:
            try:
                # print("Trying option 1 (fastest)")
                list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                normalized = False,
                                                                                                                                solver = "full",
                                                                                                                                dtype = np.float32,
                                                                                                                                epsilon = epsilons[2]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
            except:
                try:
                    # print("Trying option 2 (average)")
                    list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                    normalized = False,
                                                                                                                                    solver = "lu",
                                                                                                                                    dtype = np.float32,
                                                                                                                                    epsilon = epsilons[0]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
                except:
                    try:
                        # print("Trying option 2 (average)")
                        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                        normalized = False,
                                                                                                                                        solver = "lu",
                                                                                                                                        dtype = np.float32,
                                                                                                                                        epsilon = epsilons[1]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
                    except:
                        try:
                            # print("Trying option 2 (average)")
                            list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                            normalized = False,
                                                                                                                                            solver = "lu",
                                                                                                                                            dtype = np.float32,
                                                                                                                                            epsilon = epsilons[2]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
                        except:
                            try:
                                # print("Trying option 3 (slowest)")
                                list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                                normalized = False,
                                                                                                                                                solver = "cg",
                                                                                                                                                dtype = np.float32,
                                                                                                                                                epsilon = epsilons[0]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
                            except:
                                try:
                                    # print("Trying option 3 (slowest)")
                                    list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                                    normalized = False,
                                                                                                                                                    solver = "cg",
                                                                                                                                                    dtype = np.float32,
                                                                                                                                                    epsilon = epsilons[1]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
                                except:
                                    try:
                                        # print("Trying option 3 (slowest)")
                                        list_nodes_to_mask = [x for x in list(col.OrderedDict(sorted(nx.approximate_current_flow_betweenness_centrality(g_nx,
                                                                                                                                                        normalized = False,
                                                                                                                                                        solver = "cg",
                                                                                                                                                        dtype = np.float32,
                                                                                                                                                        epsilon = epsilons[2]).items(), key=lambda t: t[1]))) if exclude not in x and x not in prim_in][-key_size:]
                                    except:
                                        raise Exception("Failed !")
                                        return
    return list_nodes_to_mask
