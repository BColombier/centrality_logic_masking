# -*- coding: utf-8 -*-

import igraph
import networkx as nx
import os

def isolate_largest_cluster(g):
    if len(g.clusters(mode="WEAK")) > 1:
        largest_cluster = g.clusters(mode="WEAK").sizes().index(max(g.clusters(mode="WEAK").sizes()))
        g.clusters(mode="WEAK").subgraph(largest_cluster).simplify().write_gml("saved_graph.gml")
        g_nx = nx.read_gml("saved_graph.gml").to_undirected()
        os.remove("saved_graph.gml")
    else :
        g.simplify().write_gml("saved_graph.gml")
        g_nx = nx.read_gml("saved_graph.gml", label = "label").to_undirected()
        # os.remove("saved_graph.gml")
    return g_nx
