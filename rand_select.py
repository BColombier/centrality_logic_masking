import random as rd
import time as time

with open("timing_report_rand.csv", "a") as timing_file:
    for size in [1000, 5000, 10000, 50000, 100000]:
        list_nodes = range(size)
        t0 = time.clock()
        a = rd.sample(list_nodes, int(0.05*size))
        print a
        timing_file.write(str(size)+","+str(round(time.clock() - t0, 2))+"\n")