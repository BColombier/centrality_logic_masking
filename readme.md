# Centrality indicators as node selection heuristics for logic masking
#### Authors: Brice Colombier, Lilian Bossuet, David H�ly
#### Laboratoire Hubert Curien
#### 18 rue Pr. Beno�t Lauras, 42000 Saint-Etienne, FRANCE

Python source code associated to the research article: "Improving the Tradeoff Between Efficiency and Complexity for the Nodes Selection Process in Logic Masking".
