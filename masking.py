# -*- coding: utf-8 -*-
"""
Author     : Brice Colombier
Affiliation: Laboratoire Hubert Curien, UMR CNRS 5516
             University of Lyon
             18 rue du Professeur Benoit Lauras
             42000 Saint-Etienne - France
Contact    : b.colombier@univ-st-etienne.fr

Title      : Main file
Project    : Graph-based nodes selection for logic masking

File       : test.py
Last update: 2015-03-17
"""

import random
import networkx as nx
import igraph
import collections as col
import isolate_largest_cluster
import modify_nodes_mask

import heuristic_current_flow_closeness
import heuristic_approximated_current_flow_betweenness
import heuristic_current_flow_betweenness

import numpy as np

from parsers.build_bench import build as build_bench

def masking(g, prim_in, prim_out, nodes, overhead, heuristic, exclude, mod=True):

    """
    g:         input graph
    prim_in:   primary inputs of the netlist
    prim_out:  primary outputs of the netlist
    nodes:     nodes of the netlist
    overhead:  requested overhead (% extra logic gates)
    heuristic: selection heuristic to use
    exclude:   
    mod=True:  modify the graph or just return the list of nodes to modify
    """

    
    h = g.copy() #Perform deep copy
    key_size = int(len(nodes)*overhead)

    if exclude:
        if heuristic == "random":
            list_nodes_to_mask = random.sample([node for node in nodes if exclude not in node and node not in prim_in], key_size)
        elif heuristic == "current_flow_closeness":
            g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
            list_nodes_to_mask = heuristic_current_flow_closeness.heuristic_current_flow_closeness(g_nx, key_size, prim_in, exclude)
        elif heuristic == "approximated_current_flow_betweenness":
            g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
            list_nodes_to_mask = heuristic_approximated_current_flow_betweenness.heuristic_approximated_current_flow_betweenness(g_nx, key_size, prim_in, exclude)
        elif heuristic == "closeness":
            list_nodes_to_mask = [i[1] for i in sorted(zip(g.closeness(mode = "ALL"), nodes))if exclude not in i[1] and i[1] not in prim_in][-key_size:]
        elif heuristic == "betweenness":
            list_nodes_to_mask = [i[1] for i in sorted(zip(g.betweenness(directed = False), nodes)) if exclude not in i[1] and i[1] not in prim_in][-key_size:]
        elif heuristic == "current_flow_betweenness":
            g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
            list_nodes_to_mask = heuristic_current_flow_betweenness.heuristic_current_flow_betweenness(g_nx, key_size, prim_in, exclude)
        else:
            raise ValueError("Unknown heuristic")
    else:
        if heuristic == "random":
            list_nodes_to_mask = random.sample([node for node in nodes if node not in prim_in], key_size)
        elif heuristic == "current_flow_closeness":
            g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
            list_nodes_to_mask = heuristic_current_flow_closeness.heuristic_current_flow_closeness(g_nx, key_size, prim_in)
        elif heuristic == "approximated_current_flow_betweenness":
            g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
            list_nodes_to_mask = heuristic_approximated_current_flow_betweenness.heuristic_approximated_current_flow_betweenness(g_nx, key_size, prim_in)
        elif heuristic == "closeness":
            list_nodes_to_mask = [i[1] for i in sorted(zip(g.closeness(mode = "ALL"), [node for node in nodes if node not in prim_in]))[-key_size:]]
        elif heuristic == "betweenness":
            list_nodes_to_mask = [i[1] for i in sorted(zip(g.betweenness(directed = False), [node for node in nodes if node not in prim_in]))[-key_size:]]
        elif heuristic == "current_flow_betweenness":
            g_nx = isolate_largest_cluster.isolate_largest_cluster(g)
            list_nodes_to_mask = heuristic_current_flow_betweenness.heuristic_current_flow_betweenness(g_nx, key_size, prim_in)
        else:
            raise ValueError("Unknown heuristic")
        
    if mod:
        masking_key = []
        for i in list_nodes_to_mask:
            masking_key.append(random.randint(0, 1))
        h = modify_nodes_mask.modify_nodes_mask(h, list_nodes_to_mask, masking_key)
        return h, masking_key
    else:
        return list_nodes_to_mask

if __name__ == "__main__":
    for name in ["./benchmarks/c432.txt"]:
        overhead = 0.1
        for heuristic in ["current_flow_closeness",
                          "approximated_current_flow_betweenness",
                          "closeness",
                          "betweenness",
                          "current_flow_betweenness"]:
            g, prim_in, prim_out, nodes = build_bench(name)
            print heuristic
            nodes_to_mask = masking(g, prim_in, prim_out, nodes, overhead, heuristic, exclude="", mod=False)
            print nodes_to_mask
